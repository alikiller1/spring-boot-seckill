package com.itstyle.seckill.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.itstyle.seckill.util.HttpClientUtil;

public class SeckillTest {
	
	@Test
	public void test01(){
		String url="http://localhost:8080/seckill/seckill/start";
		String seckillId="1000";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("seckillId", seckillId);
		try {
		String result=HttpClientUtil.post(url, params);
		System.out.println(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void test02(){
		String url="http://localhost:8080/seckill/seckillDistributed/startRedisLock";
		String seckillId="1000";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("seckillId", seckillId);
		try {
		String result=HttpClientUtil.post(url, params);
		System.out.println(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
