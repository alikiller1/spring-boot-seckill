package com.itstyle.seckill.common.interceptor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
/**
 * 配置首页
 * 创建者 小柒2012
 * 创建时间	2017年9月7日
 */
@Configuration
public class MyAdapter extends WebMvcConfigurerAdapter{
    @Override
    public void addViewControllers( ViewControllerRegistry registry ) {
        registry.addViewController( "/" ).setViewName( "forward:/index.shtml" );
        registry.setOrder( Ordered.HIGHEST_PRECEDENCE );
        super.addViewControllers( registry );
    } 
    public static void main(String[] args) {
		List<String> list=new ArrayList<String>();
		list.add("a");
		list.add("a");
		list.add("a");
		for(String s:list){
			list.remove(s);
		}
		
		/*for(int i=0;i<list.size();i++){
			list.remove(i);
		}*/
		/*Iterator<String> it= list.iterator();
		while(it.hasNext()){
			String item= it.next();
			it.remove();
		}*/
		System.out.println(list);
	}
}
